# ethminer-wrapper

A wrapper program for [`ethminer`](https://github.com/ethereum-mining/ethminer) to work around a bug where, at a DAG transition, the hashrate may drop significantly. This wrapper program will restart ethminer when a DAG transition is detected until it is terminated.

## Dependencies

 - [Zig](https://ziglang.org)

## Building

```sh
zig build
```

## Usage

```sh
./zig-out/bin/ethminer-wrapper /path/to/ethminer --stdout -P <your-pool-URL> <other optional arguments>
```