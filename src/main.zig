const std = @import("std");
const ChildProcess = std.ChildProcess;

const logger = std.log.scoped(.ethminer_wrapper);

/// Caller must wait or kill and deinitialize the returned child process
fn createAndSpawnEthminerProcess(allocator: *std.mem.Allocator, ethminer_arguments: [][]u8) (std.mem.Allocator.Error || ChildProcess.SpawnError)!*ChildProcess {
    var ethminer_process: *ChildProcess = try ChildProcess.init(ethminer_arguments, allocator);

    // allow the process standard output to be read instead of using the parent process's standard out (the default)
    ethminer_process.stdout_behavior = ChildProcess.StdIo.Pipe;

    // start the process
    try ethminer_process.spawn();

    return ethminer_process;
}

fn destroyEthminerProcess(ethminer_process: *ChildProcess) void {
    blk: {
        logger.debug("Killing ethminer process...", .{});
        const termination: ChildProcess.Term = ethminer_process.kill() catch |e| {
            logger.err("Failed to kill ethminer process, error: {}", .{e});
            break :blk;
        };
        logger.debug("Ethminer process killed, termination: {}", .{termination});
    }
    ethminer_process.deinit();
}

pub fn main() anyerror!void {
    logger.info("Starting ethminer-wrapper...", .{});
    defer logger.info("ethminer-wrapper stopped", .{});

    // initialize the root allocator used throughout the program
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator: *std.mem.Allocator = &gpa.allocator;
    // ensure that no manual memory management errors were made during the execution of this program
    defer std.debug.assert(!gpa.deinit());

    logger.debug("Getting process arguments...", .{});
    const process_arguments: [][:0]u8 = try std.process.argsAlloc(allocator);
    logger.debug("Got process arguments", .{});
    defer std.process.argsFree(allocator, process_arguments);
    // run the given arguments to this process, assuming that they start with a valid path to ethminer are followed by valid arguments to ethminer
    const ethminer_arguments: [][:0]u8 = process_arguments[1..];

    // ignore interrupt signals in the wrapper process so that the child process can terminate gracefully
    const sigaction = std.os.Sigaction{
        // Note: SIG_IGN is the constant representing a signal handler that ignores a signal
        .handler = .{ .sigaction = std.os.SIG_IGN },
        .mask = std.os.empty_sigset,
        .flags = 0,
    };
    std.os.sigaction(std.os.SIGINT, &sigaction, null);

    // initialize the first instance of the ethminer process
    logger.debug("Initializing ethminer process...", .{});
    var ethminer_process: *ChildProcess = try createAndSpawnEthminerProcess(allocator, ethminer_arguments);
    logger.debug("Ethminer process initialized, pid: {}", .{ethminer_process.*.pid});
    // Note: all calls to spawn (notably the one in createAndSpawnEthminerProcess) must be followed by a call to wait() or kill()
    defer destroyEthminerProcess(ethminer_process);

    const stdout = std.io.getStdOut().writer();
    var stdout_buffer: [200]u8 = undefined;
    var is_not_first_dag_generation: bool = false;

    logger.info("ethminer-wrapper started", .{});
    defer logger.info("Stopping ethminer-wrapper...", .{});

    // read from the ethminer process's standard output into the standard output buffer, stopping when an error is thrown
    while (ethminer_process.stdout.?.read(&stdout_buffer) catch null) |bytes_read| {
        if (bytes_read <= 0) {
            // the end of the standard output stream has been reached
            break;
        }

        if (std.mem.indexOf(u8, &stdout_buffer, "Generating")) |keyword_index| {
            if (keyword_index < bytes_read) {
                if (is_not_first_dag_generation) {
                    // if the DAG is regenerating, restart the ethminer process to work around the bug where the regeneration will cause the hashrate to drop
                    logger.info("Re-creating ethminer process...", .{});

                    destroyEthminerProcess(ethminer_process);

                    logger.debug("Re-initializing ethminer process...", .{});
                    ethminer_process = try createAndSpawnEthminerProcess(allocator, ethminer_arguments);
                    logger.debug("Ethminer process re-initialized", .{});

                    logger.info("Ethminer process re-created", .{});

                    is_not_first_dag_generation = false;
                } else {
                    is_not_first_dag_generation = true;
                }
            }
        }
        stdout.print("W: {s}", .{stdout_buffer[0..bytes_read]}) catch logger.err("Failed to print to standard out, message: \"{s}\"", .{stdout_buffer[0..bytes_read]});
    }
}
